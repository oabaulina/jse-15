package ru.baulina.tm.constant;

public class ArgumentConst {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    public static final String INFO = "-i";

    public static final String ARGUMENTS = "-arg";

    public static final String COMMANDS = "-cmd";

}
