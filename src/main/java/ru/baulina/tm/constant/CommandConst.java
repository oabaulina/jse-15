package ru.baulina.tm.constant;

import java.lang.String;

public class CommandConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";


    public static final String TASK_CREATE = "task-create";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update_by_index";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_SHOW_BY_NAME = "task-show-by-name";

    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";


    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update_by_index";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";


    public static final String LOGIN = "login";

    public static final String LOGOUT = "logout";

    public static final String REGISTRY = "registry";

    public static final String LIST_USERS = "list-users";

    public static final String PASSWORD_CHANGE = "change-password";

    public static final String PROFILE_OF_USER = "profiler-of-user";

    public static final String PROFILE_OF_USER_CHANGE = "change-profiler-of-user";

}
