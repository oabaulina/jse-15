package ru.baulina.tm.api.service;

import ru.baulina.tm.entity.User;

import java.util.List;

public interface IAuthService {

    Long getUserId();

    void isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void changePassword(String passwordOld, String passwordNew);

    User findByLogin(String login);

    User findById();

    List<User> findAll();

    void changeUser(String email, String festName, String LastName);

}
