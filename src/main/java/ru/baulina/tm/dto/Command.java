package ru.baulina.tm.dto;

public class Command {

    private String name = "";

    private String arg = "";

    private String discription = "";

    public Command(final String name, final String arg, final String discription) {
        this.name = name;
        this.arg = arg;
        this.discription = discription;
    }

    public Command(final String name, final String arg) {
        this.name = name;
        this.arg = arg;
    }

    public Command(final String name) {
        this.name = name;
    }

    public Command() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(final String arg) {
        this.arg = arg;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(final String discription) {
        this.discription = discription;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (discription != null && !discription.isEmpty()) result.append(": ").append(discription);

        return result.toString();
    }

}
