package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.util.SystemInformation;

public final class InfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.INFO;
    }

    @Override
    public String name() {
        return CommandConst.INFO;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.INFO;
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + SystemInformation.availableProcessors);
        System.out.println("Free memory: " + SystemInformation.freeMemoryFormat);
        System.out.println("Maximum memory: " + SystemInformation.maxMemoryFormat);
        System.out.println("Total memory available to JVM: " + SystemInformation.totalMemoryFormat);
        System.out.println("Used memory by JVM: " + SystemInformation.usedMemoryFormat);
        System.out.println("OK");
        System.out.println();
    }

}
