package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

import java.util.Arrays;

public final class CommandShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.COMMANDS;
    }

    @Override
    public String name() {
        return CommandConst.COMMANDS;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.COMMANDS;
    }

    @Override
    public void execute() {
        final String[] commands = serviceLocator.getCommandService().getCommands();
        System.out.println(Arrays.toString(commands));
        System.out.println();
    }

}
