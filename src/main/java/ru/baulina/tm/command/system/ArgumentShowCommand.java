package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

import java.util.Arrays;

public final class ArgumentShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ARGUMENTS;
    }

    @Override
    public String name() {
        return CommandConst.ARGUMENTS;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.ARGUMENTS;
    }

    @Override
    public void execute() {
        final String[] arguments = serviceLocator.getCommandService().getArgs();
        System.out.println(Arrays.toString(arguments));
        System.out.println();
    }

}
