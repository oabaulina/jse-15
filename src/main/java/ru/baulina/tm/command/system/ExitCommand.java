package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.EXIT;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.EXIT;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
