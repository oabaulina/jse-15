package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.dto.Command;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.HELP;
    }

    @Override
    public String name() {
        return CommandConst.HELP;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.HELP;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Command[] commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
        System.out.println("[OK]");
        System.out.println();
    }

}
