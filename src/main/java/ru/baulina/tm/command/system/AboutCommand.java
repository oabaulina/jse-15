package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ABOUT;
    }

    @Override
    public String name() {
        return CommandConst.ABOUT;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.ABOUT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println("OK");
        System.out.println();
    }

}
