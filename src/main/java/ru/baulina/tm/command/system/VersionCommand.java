package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public final class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.VERSION;
    }

    @Override
    public String name() {
        return CommandConst.VERSION;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.VERSION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.15");
        System.out.println("OK");
        System.out.println();
    }

}
