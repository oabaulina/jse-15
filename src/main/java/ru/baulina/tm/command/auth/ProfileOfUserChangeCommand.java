package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserChangeCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROFILE_OF_USER_CHANGE;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[CHANGE_PROFILE_OF_USER]");
        System.out.println("ENTER E-MAIL: ");
        final String newEmail = TerminalUtil.nextLine();
        System.out.println("ENTER FEST NAME: ");
        final String newFestName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changeUser(newEmail, newFestName, newLastName);
        System.out.println("[OK]");
        System.out.println("");
    }

}
