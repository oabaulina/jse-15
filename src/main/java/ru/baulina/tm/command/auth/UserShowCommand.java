package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.entity.User;

import java.util.List;

public final class UserShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.LIST_USERS;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        final List<User> users = serviceLocator.getUserService().findAll();
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("PASSWORD: " + user.getPasswordHash());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFestName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
