package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PASSWORD_CHANGE;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changePassword(passwordOld, passwordNew);
        System.out.println("[OK]");
        System.out.println("");
    }

}
