package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
