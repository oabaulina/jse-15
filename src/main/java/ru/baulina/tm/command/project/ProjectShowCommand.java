package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Project;

import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROJECT_LIST;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.PROJECT_LIST;
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
