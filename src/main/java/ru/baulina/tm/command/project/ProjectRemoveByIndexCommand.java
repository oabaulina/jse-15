package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
