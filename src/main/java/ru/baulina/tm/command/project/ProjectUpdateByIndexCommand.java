package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateTaskByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
