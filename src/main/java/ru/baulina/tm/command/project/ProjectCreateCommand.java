package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.PROJECT_CREATE;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.PROJECT_CREATE;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
