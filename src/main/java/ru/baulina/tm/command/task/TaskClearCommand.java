package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.TASK_CLEAR;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.TASK_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
