package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.TASK_REMOVE_BY_INDEX;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.TASK_REMOVE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
