package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Task;

import java.util.List;

public final class TaskShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.TASK_LIST;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.TASK_LIST;
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
