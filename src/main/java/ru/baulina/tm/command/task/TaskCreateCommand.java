package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.TASK_CREATE;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.TASK_CREATE;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
