package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CommandConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String description() {
        return CommandDescriptionConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
