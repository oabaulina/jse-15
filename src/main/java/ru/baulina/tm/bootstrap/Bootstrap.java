package ru.baulina.tm.bootstrap;

import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.command.auth.*;
import ru.baulina.tm.command.project.*;
import ru.baulina.tm.command.system.*;
import ru.baulina.tm.command.task.*;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.exception.system.UnknownCommandException;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.repository.ProjectRepository;
import ru.baulina.tm.repository.TaskRepository;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.role.Role;
import ru.baulina.tm.service.*;
import ru.baulina.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ArgumentShowCommand());
        registry(new CommandShowCommand());
        registry(new ExitCommand());

        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new PasswordChangeCommand());
        registry(new ProfileOfUserCommand());
        registry(new ProfileOfUserChangeCommand());
        registry(new RegistryCommand());
        registry(new UserShowCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
            System.exit(0);
        }
        process();
    }

    private void process() {
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        for (final AbstractCommand command : commands.values()) {
            if (arg.equals(command.arg())) {
                command.execute();
                return true;
            }
        }
        throw new UnknownCommandException(arg);
    }

    private boolean parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return false;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
        return true;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
