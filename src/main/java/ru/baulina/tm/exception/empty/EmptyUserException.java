package ru.baulina.tm.exception.empty;

public class EmptyUserException extends RuntimeException{

    public EmptyUserException() {
        super("Error! User Id is empty...");
    }
}
